#!/bin/bash
set -e
## define an array ##
years=( 2013 2014 2015 2016 2017 2018 )
months=(01 02 03 04 05 06 07 08 09 10 11 12)

## get item count using ${arrayname[@]} ##
for y in "${years[@]}"
do
  echo "Starting on year: ${y}"
  # do something on $m #
  for m in ${months[@]}
  do
    echo "Starting on month: ${m} in year: ${y}"
    for d in {0..3}
    do
      echo "Packing ${y}-${m}-$d"
      if [ 0 -lt $(ls /var/taksering/${y}-${m}-$d*.csv 2> /dev/null | wc -l) ]; then
        sudo /bin/tar cvzf /var/taksering/${y}${m}$d.csv.tar /var/taksering/${y}-${m}-$d*
        sudo rm /var/taksering/${y}-${m}-$d*.csv
      else
        echo "no files from ${y}-${m}-$d"
      fi
    done
  done
done
