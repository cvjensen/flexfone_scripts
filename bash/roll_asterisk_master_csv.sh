#!/bin/bash
monthstring=`date +%Y-%m`

cd /var/taksering

mv /var/asterisk/var/log/asterisk/cdr-csv/Master.csv /var/asterisk/$monthstring.csv

/bin/tar cvzf /var/taksering/$monthstring.csv.tar ./$monthstring.csv

rm ./$monthstring.csv
