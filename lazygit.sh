function lazygit() {
    git add .
    git commit -a -m "$1"
    git push
}

echo "Please write a comment to your git commit :-)"
read comment

lazygit $comment