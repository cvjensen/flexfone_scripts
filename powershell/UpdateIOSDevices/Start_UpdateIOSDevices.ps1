#Purpose: Upgrade List of Cisco devices
#Author: CJ

#Set basedir - script home
$basedir = "C:\Scripts\UpdateIOSDevices"

#Specify newest SW version
$new_SWVersion = "1.4.8.6"

#TFTP Address for new version
$tftpaddress = "tftp://91.217.201.13/sx300_fw-1486.ros"

#If this option is $true the script will only create a report with devices and version numbers
#If this option is $false the script will create the report AND START UPDATING DEVICES!
$reportOnly = $false

#Logfile location
$logfile = "C:\temp\UpdateIOSDevices.log"

#Mail stuff
$mailto = "cj@flexfone.dk"
#$mailto = "cj@flexfone.dk, drift@flexfone.dk"
$emailFrom = "skynet@flexfone.dk"
$smtpServer = "smtp1.sippeer.dk"
$smtpUsername = ""
$smtpPassword = ""
$outputFile = "C:\temp\UpdateIOSDevices.html"
$mailSubject = "UpdateIOSDevices"
$HTMLbody = "<p>See Attachment</p>"

#Start the main script
. $basedir/main.ps1

#Create password file to store the device password - Only do this once
#read-host -assecurestring | convertfrom-securestring | out-file C:\Scripts\UpdateIOSDevices\pass.txt
