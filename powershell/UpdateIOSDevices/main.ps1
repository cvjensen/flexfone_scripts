﻿#Purpose: Upgrade List of Cisco devices
#Author: CJ

#Remember to install posh-ssh
#Find-Module posh-ssh | install-module

#Import password and devices from file 
if (!(Test-Path $basedir/pass.txt)) { 
    write-host "Password file missing - Create is with: read-host -assecurestring | convertfrom-securestring | out-file C:\Scripts\UpdateIOSDevices\pass.txt"
    exit
} elseif (!(Test-Path $basedir/Devices.txt)) {
    write-host "Devices.txt is missing - Create a list with devices in the basedir"
}
$pass = get-content $basedir\pass.txt | convertto-securestring
$device_cred = new-object -typename System.Management.Automation.PSCredential -argumentlist "admin",$pass

$devices = Get-Content $basedir\Devices.txt

Start-Transcript -path $logfile

function Invoke-IOSCommand
{
    [CmdletBinding()]
    [Alias()]
    [OutputType([int])]
    Param (
        # SSH stream to use for command execution.
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        [Renci.SshNet.ShellStream]
        $Stream,

        # Command to execute on device.
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true,
                   Position=1)]
        [string]
        $Command
    )

    Begin {
        $promptRegEx = [regex]'[\$%#>] $'
    }
    Process {
        # Discard any banner or previous command output
        do { 
            $stream.read() | Out-Null
    
        } while ($stream.DataAvailable)

        $stream.writeline($Command)

        #discard line with command entered
        $Stream.ReadLine() | Out-Null
        Start-Sleep -Milliseconds 500

        $out = ''

        # read all output until there is no more
        do { 
            $out += $stream.read()
    
        } while ($stream.DataAvailable)

        $outputLines = $out.Split("`n")
        foreach ($line in $outputLines) {
            if ($line -notmatch $promptRegEx) {
                $line
            }
        }
    }
    End{}
}

$device_list = @()
$ErrorMessage = ""

Foreach ($device in $devices) {
    $ErrorMessage = ""
    try {
        New-SSHSession -ComputerName $device -Credential $device_cred -AcceptKey -ErrorAction Stop
        $session = Get-SSHSession -Index 0
        $stream = $session.Session.CreateShellStream("dumb", 0, 0, 0, 0, 1000)
        $out = ''
        Start-Sleep -Milliseconds 1000
        do { 
            $out += $stream.read()
    
        } while ($stream.DataAvailable)
        #Checking if device is asking to change old password
        if ($out -like "*Please change the password for better protection*") {
            #write-host "Your password has exceeded the maximum lifetime. Please change the password for better protection of your network."
            Invoke-IOSCommand -stream $stream -command 'N'
        }
        $out = Invoke-IOSCommand -stream $stream -command 'show version'
        $out = $out -split "`n" 
        $SWVersion = ($out | select-string -pattern "(\d+).(\d+).(\d+).(\d+)")[0].Matches.Value
        $BootVersion = ($out | select-string -pattern "(\d+).(\d+).(\d+).(\d+)")[1].Matches.Value
        $HWVersion = ($out | select-string -pattern "V(\d+)")[0].Matches.Value

    } catch {
        $ErrorMessage = $_.Exception.Message
    }

    if ($ErrorMessage -notlike "") {
        $device_object = New-Object -TypeName PSObject
        $device_object | Add-Member -MemberType NoteProperty –Name Device –Value $device
        $device_object | Add-Member –MemberType NoteProperty –Name SWVersion –Value "Error"
        $device_object | Add-Member –MemberType NoteProperty –Name BootVersion –Value "Error"
        $device_object | Add-Member –MemberType NoteProperty –Name HWVersion –Value "Error"
        $device_object | Add-Member –MemberType NoteProperty –Name NeedUpgrade –Value $false
        $device_object | Add-Member –MemberType NoteProperty –Name ConnectionError –Value $ErrorMessage
    } else {
        $device_object = New-Object -TypeName PSObject
        $device_object | Add-Member -MemberType NoteProperty –Name Device –Value $device
        $device_object | Add-Member –MemberType NoteProperty –Name SWVersion –Value $SWVersion
        $device_object | Add-Member –MemberType NoteProperty –Name BootVersion –Value $BootVersion
        $device_object | Add-Member –MemberType NoteProperty –Name HWVersion –Value $HWVersion
        if ([System.Version]$new_SWVersion -gt [System.Version]$SWVersion) {
            $device_object | Add-Member –MemberType NoteProperty –Name NeedUpgrade –Value $true
        } else {
            $device_object | Add-Member –MemberType NoteProperty –Name NeedUpgrade –Value $false
        }
        $device_object | Add-Member –MemberType NoteProperty –Name ConnectionError –Value "N/A"
    }
    $device_list += $device_object
    Remove-SSHSession 0
}

if (!($reportOnly)) {
    write-host Starting to update devices!
    Foreach ($device_object in $device_list) {
        if ($device_object.NeedUpgrade) {
            $device_object | Add-Member -MemberType NoteProperty –Name StartingUpgrade –Value $true
            #Preparing and starting the upgrade procedure
            New-SSHSession -ComputerName $device_object.Device -Credential $device_cred -AcceptKey
            $session = Get-SSHSession -Index 0
            $stream = $session.Session.CreateShellStream("dumb", 0, 0, 0, 0, 1000)
            $bootvar = Invoke-IOSCommand -stream $stream -command 'show bootvar'
            $bootvar = $bootvar -split "`n"
            $image1 = ($bootvar | select-string -pattern "image-")[0]
            $image2 = ($bootvar | select-string -pattern "image-")[1]
            if ($image1 -like "*Not active*") {
                $targetimage = "image-1"
            } else {
                $targetimage = "image-2"
            }
            #For this command success is "The copy operation was completed successfully"
            Invoke-IOSCommand -stream $stream -command "copy $tftpaddress $targetimage"
            $copysuccess = $false
            $x = 1
            DO {
                $out = ''
                do { 
                    $out += $stream.read()
    
                } while ($stream.DataAvailable)
                write-host $out
                if ($out -like "*Copy:*bytes copied in*") {
                    $copysuccess = $true
                    break
                }
                write-host "Still waiting for copy to finish..."
                sleep 10
                $x++
            } While ($x -le 30)

            if ($copysuccess) {

                Invoke-IOSCommand -stream $stream -command "boot system $targetimage"
                Invoke-IOSCommand -stream $stream -command "reload"
                Invoke-IOSCommand -stream $stream -command "y"
                Invoke-IOSCommand -stream $stream -command "y"
                Remove-SSHSession 0

                #Waiting for the switch to answer
                write-host "Waiting for SSH connection..."
                $answer = $false
                $x = 1
                DO
                {
                 sleep 10
                 New-SSHSession -ComputerName $device_object.Device -Credential $device_cred -AcceptKey
                 If($?) {
                    $answer = $true
                    break
                    }
                 $x++
                 write-host "Waiting for SSH connection..."
                } While ($x -le 6)

                if ($answer) {
                    #Check new version
                    $session = Get-SSHSession -Index 0
                    $stream = $session.Session.CreateShellStream("dumb", 0, 0, 0, 0, 1000)
                    $out = Invoke-IOSCommand -stream $stream -command 'show version'
                    $out = $out -split "`n" 
                    $out | select-string -pattern "(\d+).(\d+).(\d+).(\d+)"
                    $SWVersion = ($out | select-string -pattern "(\d+).(\d+).(\d+).(\d+)")[0].Matches.Value
                    if ($SWVersion -like $new_SWVersion) {
                        #Success
                        $device_object | Add-Member -MemberType NoteProperty –Name UpgradeError –Value "N/A"
                        $device_object | Add-Member -MemberType NoteProperty –Name UpgradeStatus –Value "Success - New version $SWVersion"
                    } else {
                        #Current version is not like new version 
                        $device_object | Add-Member -MemberType NoteProperty –Name UpgradeError –Value "Current SWVersion($SWVersion) is not new version ($new_SWVersion)"
                        $device_object | Add-Member -MemberType NoteProperty –Name UpgradeStatus –Value "Error"
                    }
                    Remove-SSHSession 0
                } else {
                    #Couldn't get connection to device - ERROR
                    $device_object | Add-Member -MemberType NoteProperty –Name UpgradeError –Value "Couldn't get connection to device"
                    $device_object | Add-Member -MemberType NoteProperty –Name UpgradeStatus –Value "Error"
                }


            } else {

                $device_object | Add-Member -MemberType NoteProperty –Name UpgradeError –Value "Copy process timed out"
                $device_object | Add-Member -MemberType NoteProperty –Name UpgradeStatus –Value "Error"

            }

        } else {
            $device_object | Add-Member -MemberType NoteProperty –Name StartingUpgrade –Value $false
            $device_object | Add-Member -MemberType NoteProperty –Name UpgradeError –Value "N/A"
            $device_object | Add-Member -MemberType NoteProperty –Name UpgradeStatus –Value "N/A"
        }
    }
}

$date = date
$device_html = $device_list | sort-object -Property { [System.Version]$_.Device } | ConvertTo-HTML -fragment

$HTMLmessage = @"
<font color=""black"" face=""Arial"" size=""3"">
<style type=""text/css"">body{font: .8em ""Lucida Grande"", Tahoma, Arial, Helvetica, sans-serif;}
table { 
    width: 1200px; 
    border-collapse: collapse; 
    margin:50px auto;
    }
/* Zebra striping */
tr:nth-of-type(odd) { 
    background: #eee; 
    }
th { 
    background: #349AB5; 
    color: white; 
    font-weight: bold; 
    }
td, th { 
    padding: 6px; 
    border: 1px solid #ccc; 
    text-align: left; 
    font-size: 16px;
    }
</style>
<body BGCOLOR=""white"">
    <div align="center">
        <h1 style='font-family:arial;'><b>Update IOS Devices</b></h1>
        <p>Report generated: $date</p>
        $device_html
    </div>  
</body>
"@

$HTMLmessage | out-file $outputFile

Stop-Transcript

$att1 = new-object Net.Mail.Attachment($outputFile)
$att2 = new-object Net.Mail.Attachment($logfile)
$smtp=New-Object Net.Mail.SmtpClient-ArgumentList $smtpServer
$smtp.credentials=New-Object System.Net.NetworkCredential($smtpUsername,$smtpPassword);
$msg=New-Object Net.Mail.MailMessage
$msg.From=$emailFrom
$msg.To.Add($mailto)
$msg.Subject=$mailSubject
$msg.IsBodyHTML=$true
$msg.Body=$HTMLbody
$msg.Attachments.Add($att1)
$msg.Attachments.Add($att2)
$smtp.Send($msg)
$msg.Dispose()
