﻿#Purpose: Get vmware tools information
#Author: CJ

$date = date
$hostname = hostname

$Result = Get-VM | Where-Object { $_.PowerState -like "PoweredOn" } | Select @{L='GuestName';E={($_.Name)}}, 
            @{L='OperatingSystem';E={((($_).Guest).OSFullName)}}, 
            @{L='Machine V.';E={($_.Version)}}, 
            PowerState, 
            @{L='BootTime';E={(((($_).ExtensionData).RunTime).BootTime)}}, 
            @{L='InstallerMounted';E={(((($_).ExtensionData).RunTime).ToolsInstallerMounted)}}, 
            @{L='ToolsVersion';E={(((($_).Guest).ExtensionData).ToolsVersion)}}, 
            @{L='VersionStatus';E={(((($_).Guest).ExtensionData).ToolsVersionStatus)}},  
            @{L='RunningStatus';E={(((($_).Guest).ExtensionData).ToolsRunningStatus)}}, 
            @{L='UpgradePolicy';E={((((($_).ExtensionData).Config).Tools).ToolsUpgradePolicy)}} 


#Tools need upgrade
$ToolsNeedUpgrade = $Result | Where-Object { $_.VersionStatus -like "guestToolsNeedUpgrade" } | sort-object GuestName | ConvertTo-HTML -fragment

#Tools not running
$ToolsNotRunning = $Result | Where-Object { $_.RunningStatus -like "guestToolsNotRunning" } | sort-object GuestName | ConvertTo-HTML -fragment

#Unmanaged machines
$ToolsUnmanaged = $Result | Where-Object { $_.RunningStatus -like "guestToolsRunning" -and $_.VersionStatus -like "guestToolsUnmanaged" } | sort-object GuestName | ConvertTo-HTML -fragment

#All other machines that are powered on, tools is running and version is up-to-date
$ToolsOK = $Result | Where-Object { $_.RunningStatus -like "guestToolsRunning" -and $_.VersionStatus -like "guestToolsCurrent" } | sort-object GuestName | ConvertTo-HTML -fragment


$HTMLmessage = @"
<font color=""black"" face=""Arial"" size=""3"">
<style type=""text/css"">body{font: .8em ""Lucida Grande"", Tahoma, Arial, Helvetica, sans-serif;}
ol{margin:0;}
table{width:100%}
thead{}
thead th{font-size:120%;}
th{border-bottom:2px solid rgb(79,129,189);border-top:2px solid rgb(79,129,189);padding-bottom:10px;padding-top:10px;}
tr{padding:10px 10px 10px 10px;border:none;}
td:first-child + td{white-space: nowrap; text-overflow:ellipsis; overflow: hidden; max-width:1px;}
div{margin:20px;padding:20px;border-radius:25px;}
#middle{background-color:#900;}
</style>
<body BGCOLOR=""white"">
<div align="center">
<h1 style='font-family:arial;'><b>Vmware Tools Report</b></h1>
<p>Report generated: $date</p>
<div style='background-color: #CD6155'>
<h3>Tools not running</h3>
$ToolsNotRunning
</div>
<div style='background-color: #F4D03F'>
<h3>Tools that need upgrade</h3>
$ToolsNeedUpgrade
</div>
<div style='background-color: #CCD1D1'>
<h3>Tools Unmanaged (Should only be linux)</h3>
$ToolsUnmanaged
</div>
<div style='background-color: #52BE80'>
<h3>Tools OK</h3>
$ToolsOK
</div>
</div>
</body>
"@

$HTMLbody = "<p>See Attachment</p>"

$HTMLmessage | out-file "C:\temp\Get-VmwareToolsReport.html" -Force

$mailto = "cj@flexfone.dk"
$emailFrom = "$hostname@flexfone.dk"
$smtpServer = "smtp1.sippeer.dk"
$smtpUsername = ""
$smtpPassword = ""
$file = "C:\temp\Get-VmwareToolsReport.html"
$att = new-object Net.Mail.Attachment($file)

$smtp=New-Object Net.Mail.SmtpClient-ArgumentList $smtpServer
$smtp.credentials=New-Object System.Net.NetworkCredential($smtpUsername,$smtpPassword);
$msg=New-Object Net.Mail.MailMessage
$msg.From=$emailFrom
$msg.To.Add($mailto)
$msg.Subject="Vmware Tools Report"
$msg.IsBodyHTML=$true
$msg.Body=$HTMLbody
$msg.Attachments.Add($att)
$smtp.Send($msg)
$msg.Dispose()