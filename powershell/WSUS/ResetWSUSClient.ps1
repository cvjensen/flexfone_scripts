﻿gpupdate /force
net stop wuauserv
Set-Service -Name wuauserv -StartupType Automatic
Remove-ItemProperty -Path "HKLM:\Software\Microsoft\Windows\CurrentVersion\WindowsUpdate" -Name "SusClientId"
Remove-ItemProperty -Path "HKLM:\Software\Microsoft\Windows\CurrentVersion\WindowsUpdate" -Name "SusClientIDValidation"
net start wuauserv
wuauclt.exe /resetauthorization /detectnow
wuauclt.exe /reportnow