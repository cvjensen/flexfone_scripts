#Purpose: Automation of WSUS update approval
#Author: CJ

Start-transcript -path "C:\temp\Automate_WSUS.log" -Force

write-host "Loading WSUS plugin and connecting to WSUS server..."
#Load WSUS stuff
[reflection.assembly]::LoadWithPartialName("Microsoft.UpdateServices.Administration") | out-null
#Connect WSUS server
$wsus = [Microsoft.UpdateServices.Administration.AdminProxy]::getUpdateServer('localhost',$False,8530)

If ($wsus)
{
  #Find next monday
  Write-host "Calculating monday and tuesday..."
  $date = Get-Date
  $date = $date.AddDays(1)
  while($date.DayOfWeek -notlike "Monday") {
      $date = $date.AddDays(1)
  }
  $monday = $date
  $tuesday = $date.AddDays(1)
  write-host "Target monday must be: $monday and target tuesday: $tuesday"

  #Date objects
  write-host "Adjusting some time stuff..."
  $m0100_date = Get-Date $monday -Hour 1 -Minute 0 -Second 0 -Millisecond 0
  $m0200_date = Get-Date $monday -Hour 2 -Minute 0 -Second 0 -Millisecond 0
  $m0300_date = Get-Date $monday -Hour 3 -Minute 0 -Second 0 -Millisecond 0
  $m0400_date = Get-Date $monday -Hour 4 -Minute 0 -Second 0 -Millisecond 0
  $t0100_date = Get-Date $tuesday -Hour 1 -Minute 0 -Second 0 -Millisecond 0
  $t0200_date = Get-Date $tuesday -Hour 2 -Minute 0 -Second 0 -Millisecond 0
  $t0300_date = Get-Date $tuesday -Hour 3 -Minute 0 -Second 0 -Millisecond 0
  $t0400_date = Get-Date $tuesday -Hour 4 -Minute 0 -Second 0 -Millisecond 0

  #Define groups
  write-host "Getting computer target groups from WSUS..."
  $m0100_group = $wsus.GetComputerTargetGroups() | where {$_.Name -eq 'M0100'}
  $m0200_group = $wsus.GetComputerTargetGroups() | where {$_.Name -eq 'M0200'}
  $m0300_group = $wsus.GetComputerTargetGroups() | where {$_.Name -eq 'M0300'}
  $m0400_group = $wsus.GetComputerTargetGroups() | where {$_.Name -eq 'M0400'}
  $t0100_group = $wsus.GetComputerTargetGroups() | where {$_.Name -eq 'T0100'}
  $t0200_group = $wsus.GetComputerTargetGroups() | where {$_.Name -eq 'T0200'}
  $t0300_group = $wsus.GetComputerTargetGroups() | where {$_.Name -eq 'T0300'}
  $t0400_group = $wsus.GetComputerTargetGroups() | where {$_.Name -eq 'T0400'}

  #Create some objects
  write-host "Creating update group objects..."
  $updateGroups = @()
  $updateGroups += [pscustomobject]@{GroupName='m0100';DateTime=$m0100_date;TargetGroup=$m0100_group}
  $updateGroups += [pscustomobject]@{GroupName='m0200';DateTime=$m0200_date;TargetGroup=$m0200_group}
  $updateGroups += [pscustomobject]@{GroupName='m0300';DateTime=$m0300_date;TargetGroup=$m0300_group}
  $updateGroups += [pscustomobject]@{GroupName='m0400';DateTime=$m0400_date;TargetGroup=$m0400_group}
  $updateGroups += [pscustomobject]@{GroupName='t0100';DateTime=$t0100_date;TargetGroup=$t0100_group}
  $updateGroups += [pscustomobject]@{GroupName='t0200';DateTime=$t0200_date;TargetGroup=$t0200_group}
  $updateGroups += [pscustomobject]@{GroupName='t0300';DateTime=$t0300_date;TargetGroup=$t0300_group}
  $updateGroups += [pscustomobject]@{GroupName='t0400';DateTime=$t0400_date;TargetGroup=$t0400_group}

  #Get all updates not declined
  write-host "Getting all updates not declined..."
  $updates = $wsus.GetUpdates() | Where-Object {$_.IsDeclined -match "false"}

  #Approve updates for each group
  write-host "Approving all updates for each update group..."
  Foreach ($group in $updateGroups) {
    Foreach ($update in $updates)
    {
      Try
      {
          $update.Approve("Install",$group.TargetGroup,$group.DateTime) | Out-Null
      }
      Catch
      {
          $ErrorMessage = $_.Exception.Message
          write-host $ErrorMessage
      }
    }
  }
  $fragment = $updateGroups | select GroupName,DateTime | ConvertTo-HTML -fragment
} else {
  $fragment = "<h2>Something went wrong - couldn't connect to WSUS!</h2>"
}

#Build final output from logs
write-host "Building mail object and sending mail notify..."
$now = date
$body = @"
<font color=""black"" face=""Arial"" size=""3"">
<style type=""text/css"">body{font: .8em ""Lucida Grande"", Tahoma, Arial, Helvetica, sans-serif;}
table {
    width: 1200px;
    border-collapse: collapse;
    margin:30px auto;
    }
/* Zebra striping */
tr:nth-of-type(odd) {
    background: #eee;
    }
th {
    background: #349AB5;
    color: white;
    font-weight: bold;
    }
td, th {
    padding: 2px;
    border: 1px solid #ccc;
    text-align: left;
    font-size: 16px;
    }
</style>
<body BGCOLOR=""white"">
    <div align="center">
        <h1 style='font-family:arial;'><b>WSUS</b></h1>
        <p>Report generated: $now</p>
        $fragment
        <p>Please check the logfile</p>
    </div>
</body>
"@

#When all jobs are done - send mail...
send-mailmessage -from "wsus01@flexfone.dk" -to "cj@flexfone.dk" -subject "WSUS Update Approval Automation" -body $body -BodyAsHtml -Attachment "C:\temp\Automate_WSUS.log" -smtpServer smtp1.sippeer.dk

#Dropping mail body to html file...
$body | Out-File "C:\temp\Automate_WSUS.html" -Force

Stop-transcript
