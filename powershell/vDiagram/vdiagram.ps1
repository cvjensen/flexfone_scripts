Param ($VIServer=$FALSE, $Cluster=$FALSE)

#############################################################
#															#
#    vDiagram script by Alan Renouf - Virtu-Al				#
#    Blog: http://teckinfo.blogspot.com/					#
#															#
#    Usage: vDiagram.ps1 -VIServer MYVISERVER				#
#															#
#    Optional paramater of -Cluster MYCLUSTER 				#
#		                                                   #
#
# Shape file needs to be in 'My Documents\My Shapes' 		#
# folder													#
#															#
# Set the $Savefile as the location you would like the file #
# to be saved												#
#############################################################
$SaveFile = [system.Environment]::GetFolderPath('MyDocuments') + "\My_vDrawing.vsd"
if ($VIServer -eq $FALSE) { $VIServer = Read-Host "Please enter a Virtual Center name or ESX Host to diagram:" }

$shpFile = "\My-VI-Shapes.vss"


function connect-visioobject ($firstObj, $secondObj)
{
	$shpConn = $pagObj.Drop($pagObj.Application.ConnectorToolDataObject, 0, 0)

	#// Connect its Begin to the 'From' shape:
	$connectBegin = $shpConn.CellsU("BeginX").GlueTo($firstObj.CellsU("PinX"))
	
	#// Connect its End to the 'To' shape:
	$connectEnd = $shpConn.CellsU("EndX").GlueTo($secondObj.CellsU("PinX"))
}

function add-visioobject ($mastObj, $item)
{
 		Write-Host "Adding $item"
		# Drop the selected stencil on the active page, with the coordinates x, y
  		$shpObj = $pagObj.Drop($mastObj, $x, $y)
		# Enter text for the object
  		$shpObj.Text = $item
		#Return the visioobject to be used
		return $shpObj
 }

# Create an instance of Visio and create a document based on the Basic Diagram template.
$AppVisio = New-Object -ComObject Visio.Application
$docsObj = $AppVisio.Documents
$DocObj = $docsObj.Add("Basic Diagram.vst")

# Set the active page of the document to page 1
$pagsObj = $AppVisio.ActiveDocument.Pages
$pagObj = $pagsObj.Item(1)

# Connect to the VI Server
Write-Host "Connecting to $VIServer"
$VIServer = Connect-VIServer $VIServer

# Load a set of stencils and select one to drop
$stnPath = [system.Environment]::GetFolderPath('MyDocuments') + "\My Shapes"
$stnObj = $AppVisio.Documents.Add($stnPath + $shpFile)
$VCObj = $stnObj.Masters.Item("Virtual Center Management Console")
$HostObj = $stnObj.Masters.Item("ESX Host")
$MSObj = $stnObj.Masters.Item("Microsoft Server")
$LXObj = $stnObj.Masters.Item("Linux Server")
$OtherObj =  $stnObj.Masters.Item("Other Server")
$CluShp = $stnObj.Masters.Item("Cluster")

If ((Get-Cluster) -ne $Null){

	If ($Cluster -eq $FALSE){ $DrawItems = get-cluster }Else {$DrawItems = (Get-Cluster $Cluster)}
	
	$x = 0
	$VCLocation = $DrawItems | Get-VMHost
	$y = $VCLocation.Length * 1.50 / 2
	
	$VCObject = add-visioobject $VCObj $VIServer
	
	$x = 1.50
	$y = 1.50
	
	ForEach ($Cluster in $DrawItems)
	{
		$cluster_object = Get-Cluster $Cluster
		$cluster_name = $cluster_object.name 
		$cluster_ha_state = $cluster_object.HAEnabled
		$cluster_ha_level = $cluster_object.HAFailoverLevel
		$cluster_drs_state = $cluster_object.DrsEnabled
		$cluster_drs_level = $cluster_object.DrsAutomationLevel

		$cluster_string = "$cluster_name `nHA Enabled: $cluster_ha_state `nHA Failover level: $cluster_ha_level `nDRS Enabled: $cluster_drs_state `nDRS level: $cluster_drs_level"

		$cluster_string

		$CluVisObj = add-visioobject $CluShp $cluster_string
		connect-visioobject $VCObject $CluVisObj
		
		$x=6.00
		ForEach ($VMHost in (Get-Cluster $Cluster | Get-VMHost))
		{
			$VMHostView = Get-Cluster $Cluster | Get-VMHost $VMHost | Get-View

			$VMHost_CPUSockets = $VMHostView.hardware.cpuinfo.numCpuPackages
			$VMHost_CPUCores = $VMHostView.hardware.cpuinfo.numCpuCores
			$VMHost_MemorySizeGB = [System.Math]::Round($VMHostView.hardware.memorysize / 1024Mb)

			$VMHost_string = "`n`n`n$VMhost `nCPU Sockets: $VMHost_CPUSockets `nCPU Cores: $VMHost_CPUCores `nMemory: $VMHost_MemorySizeGB GB"

			$Object1 = add-visioobject $HostObj $VMHost_string
			$Host_Object = $Object1
			connect-visioobject $CluVisObj $Object1

			$count = 1
			ForEach ($VM in (Get-vmhost $VMHost | get-vm))
			{		

				$VM_info = $VM | Select-Object Name, MemoryGB, NumCpu, @{N="Datastore";E={Get-Datastore -VM $_}}, @{N="IP";E={@($_.guest.IPAddress[0])}}, @{ n="DiskUsedGB"; e={[math]::round( $_.UsedSpaceGB )}}, @{ n="TotalHDSizeGB"; e={[math]::round((Get-HardDisk -vm $_ | Measure-Object -Sum CapacityGB).Sum)}}, @{n="Network"; e={(Get-NetworkAdapter -VM $_ | sort-object NetworkName | Select -unique -expand NetworkName) -join ', '}}

				$VM_name = $VM_info.Name
				$VM_cpu = $VM_info.NumCpu
				$VM_memory = $VM_info.MemoryGB
				$VM_totalhd = $VM_info.TotalHDSizeGB
				$VM_datastore = $VM_info.Datastore
				$VM_network = $VM_info.Network
				$VM_ip = $VM_info.IP

				If ($vm.Guest.OSFUllName -eq $Null) {
					$VM_string = "$VM_name `nvCPU: $VM_cpu / Mem: $VM_memory GB `n$VM_totalhd GB / $VM_datastore `n$VM_network / $VM_ip"
				} else {
				    if ($vm.Guest.OSFUllName.contains("Microsoft") -eq $True) {
						$VM_string = "`n`n$VM_name `nvCPU: $VM_cpu / Mem: $VM_memory GB `n$VM_totalhd GB / $VM_datastore `n$VM_network / $VM_ip"
					} else {
					    $VM_string = "$VM_name `nvCPU: $VM_cpu / Mem: $VM_memory GB `n$VM_totalhd GB / $VM_datastore `n$VM_network / $VM_ip"
					}
				}

				if ($count -eq 10) {

					$y += 2
					$x = 11
					If ($vm.Guest.OSFUllName -eq $Null)
					{
						$Object2 = add-visioobject $OtherObj $VM_string
					}
					Else
					{
						If ($vm.Guest.OSFUllName.contains("Microsoft") -eq $True)
						{
							$Object2 = add-visioobject $MSObj $VM_string
						}
						else
						{
							$Object2 = add-visioobject $LXObj $VM_string
						}
					}

					$count = 1
					connect-visioobject $Host_Object $Object2
				} else {

					$x += 5
					If ($vm.Guest.OSFUllName -eq $Null)
					{
						$Object2 = add-visioobject $OtherObj $VM_string
					}
					Else
					{
						If ($vm.Guest.OSFUllName.contains("Microsoft") -eq $True)
						{
							$Object2 = add-visioobject $MSObj $VM_string
						}
						else
						{
							$Object2 = add-visioobject $LXObj $VM_string
						}
					}

				    connect-visioobject $Object1 $Object2
				}
				$count++	
				$Object1 = $Object2
			}
			$x = 6.00
			$y += 2
		}
	$x = 1.50
	}
}
Else
{
	$DrawItems = Get-VMHost
	
	$x = 0
	$y = $DrawItems.Length * 1.50 / 2
	
	$VCObject = add-visioobject $VCObj $VIServer
	
	$x = 1.50
	$y = 1.50
	
	ForEach ($VMHost in $DrawItems)
	{
		$Object1 = add-visioobject $HostObj $VMHost
		connect-visioobject $VCObject $Object1
		ForEach ($VM in (Get-vmhost $VMHost | get-vm))
		{		
			$x += 1.50
			If ($vm.Guest.OSFUllName -eq $Null)
			{
				$Object2 = add-visioobject $OtherObj $VM
			}
			Else
			{
				If ($vm.Guest.OSFUllName.contains("Microsoft") -eq $True)
				{
					$Object2 = add-visioobject $MSObj $VM
				}
				else
				{
					$Object2 = add-visioobject $LXObj $VM
				}
			}	
			connect-visioobject $Object1 $Object2
			$Object1 = $Object2
		}
		$x = 1.50
		$y += 1.50
	}
$x = 1.50
}

# Resize to fit page
$pagObj.ResizeToFitContents()

# Zoom to 50% of the drawing - Not working yet
#$Application.ActiveWindow.Page = $pagObj.NameU
#$AppVisio.ActiveWindow.zoom = [double].5

# Save the diagram
$DocObj.SaveAs("$Savefile")

# Quit Visio
#$AppVisio.Quit()
Write-Output "Document saved as $savefile"
Disconnect-VIServer -Server $VIServer -Confirm:$false