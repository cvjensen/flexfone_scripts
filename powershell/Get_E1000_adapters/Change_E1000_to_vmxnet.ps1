﻿$servers = @(
"terminal.sippeer.dk"
)

Foreach ($server in $servers) {
    write-host "Changing adapter on: " $server

    Shutdown-VMGuest $server -Confirm:$False

    #Waiting for Shutdownn to complete
    do {                                                                                                                                                                                                                                                                                      
        Write-Host "Waiting on shutdown for VM: " $server
        $GetVM = Get-VM $server
        $Status = $GetVM.Powerstate
        Write-Host "PowerStatus is:" $Status
        sleep 5
    }until($GetVM.Powerstate -eq "PoweredOff")

    get-VM  $server | get-Networkadapter | Set-Networkadapter -type vmxnet3 -Confirm:$False

    get-VM  $server | get-Networkadapter
    write-host "Check NIC - Press enter to start server again..."
    pause
    Start-VM $server
}

