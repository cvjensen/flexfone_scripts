﻿$servers = @(
"ser01.test.sippeer.dk"
)

$pass = Read-Host -AsSecureString

$source = "C:\scripts\flexfone_scripts\powershell\Get_E1000_adapters\configure_new_adapter_linux.sh"
$destination = "/root/configure_new_adapter.sh"


Foreach ($server in $servers) {
    write-host "Changing adapter on: " $server

    Shutdown-VMGuest $server -Confirm:$False

    #Waiting for Shutdownn to complete
    $x = 0
    do {                                                                                                                                                                                                                                                                                      
        Write-Host "Waiting on shutdown for VM: " $server " - Will poweroff after 20 seconds..."
        $GetVM = Get-VM $server
        $Status = $GetVM.Powerstate
        Write-Host "PowerStatus is:" $Status
        sleep 5
        $x++
    }until($GetVM.Powerstate -eq "PoweredOff" -or $x -like 4)
    if ($Status -notlike "PoweredOff") {
        Stop-VMGuest -kill $server -Confirm:$False
        sleep 5
    }

    get-VM  $server | get-Networkadapter | Set-Networkadapter -type vmxnet3 -Confirm:$False

    get-VM  $server | get-Networkadapter
    write-host "Check NIC - Press enter to start server again..."
    pause
    Start-VM $server
    write-host "Sleeping 20 seconds - waiting for server to startup..."
    sleep 20
    Copy-VMGuestFile -LocalToGuest -VM $server -Source "$source" -Destination "$destination" -GuestUser root -GuestPassword $pass -Verbose -Force
    Invoke-VMScript -ScriptText "chmod +x $destination" -VM $server -GuestUser root -GuestPassword $pass -ScriptType Bash
    Invoke-VMScript -ScriptText "$destination" -VM $server -GuestUser root -GuestPassword $pass -ScriptType Bash

}


