﻿#Get all windows servers with e1000 adapter
$servers = Get-VM | Get-NetworkAdapter | Where-object {$_.Type -ne "Vmxnet3" -and $_.parent.ExtensionData.config.GuestFullName -notlike "*Windows*" } | `
Select `
@{N="VM";E={$_.Parent.Name}},`
Name,Type,`
@{N="Configured OS";E={$_.Parent.ExtensionData.config.GuestFullName}},`
@{N="PowerState";E={$_.parent.Powerstate}},`
@{N="Network";E={$_.parent.ExtensionData.Guest.Net.Network}},`
@{N="IP";E={
        if ($_.parent.ExtensionData.Guest.Net.IpConfig.IpAddress[0].PrefixLength -lt 32) {
            $nic = 0
        } else {
            $nic = 1
        }
        $_.parent.ExtensionData.Guest.Net.IpAddress[$nic]
    }},`
@{N="Subnet Mask";E={
        if ($_.parent.ExtensionData.Guest.Net.IpConfig.IpAddress[0].PrefixLength -lt 32) {
            $nic = 0
        } else {
            $nic = 1
        }
        $dec = [Convert]::ToUInt32($(("1" * $_.parent.ExtensionData.Guest.Net.IpConfig.IpAddress[$nic].PrefixLength).PadRight(32, "0")), 2)
        $DottedIP = $( For ($i = 3; $i -gt -1; $i--) {
          $Remainder = $dec % [Math]::Pow(256, $i)
          ($dec - $Remainder) / [Math]::Pow(256, $i)
          $dec = $Remainder
         } )
        [String]::Join('.', $DottedIP) 
    }}

if ($_.parent.ExtensionData.Guest.Net.IpConfig.IpAddress[0].PrefixLength -lt 32) {
     $nic = 0
} else {
     $nic = 0
}

$servers
