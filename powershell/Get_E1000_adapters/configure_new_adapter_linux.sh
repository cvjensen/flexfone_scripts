#Checking if interface is already working.
if ping -c1 8.8.8.8 1>/dev/null 2>/dev/null
then
  echo "Connection success - quitting script!"
else
  echo "No connection - Trying to reconfigure interface!"
  #Define old and new interface names
  cd /etc/sysconfig/network-scripts
  devices=$(ls | grep -Po 'ifcfg-...[0-9]{1,4}')
  for i in $devices; do
      echo "Checking: $i"
      if [[ $(cat $i | grep "IPADDR") ]]; then
        echo "Found IPADDR in $i - setting old device to be $i"
        oldfile=$i
      fi
  done

  oldint=$(printf '%s\n' "${oldfile//ifcfg-/}")
  newint=$(ip addr | grep -Po 'eth[0-9]{1,4}|ens[0-9]{1,4}' | head -n 1)
  newfile="ifcfg-$newint"

  #Copy old config to new config /etc/sysconfig/network-scripts
  cp $oldfile $newfile

  #Replace both DEVICE and NAME
  sed -i -e "s/$oldint/$newint/g" $newfile

  #Delete UUID from config
  sed -i '/UUID/d' $newfile

  #ifdown ifup
  ifdown $newint && ifup $newint

  #Test
  echo "Testing network connection!"
  ping -c 4 dr.dk
fi
