﻿######################
#Change administrator password on Windows using powercli and Invoke-VMScript
#Author: CJ
######################

import-module VMware.VimAutomation.Core

Connect-VIServer vcenter09.virtualdatacenter.nu

$servers = @(
    "beta-live.sippeer.dk"
)

$user = "admin"
$new_pass = "FLY!2"
$old_pass = "fisk"

$script = @'
$hostname = hostname
$new_pass = "#new_pass#"
write-host "Setting password for: #user# to $hostname$new_pass on $hostname"
$localadministrator=[adsi]("WinNT://./#user#, user")
$localadministrator.psbase.invoke("SetPassword", "$hostname$new_pass")
'@

$script = $script.Replace('#new_pass#',$new_pass).Replace('#user#',$user)

foreach ($server in $servers) {
    write-host "Running script at: $server"
    $ErrorMessage = ""
    #Create hostname variable without fqdn
    try {
        $short_hostname = $server.Substring(0, $server.IndexOf('.'))
    } catch {
        write-host "No dot in server name - setting short_hostname to server variable"
        $short_hostname = $server
    }
    #Change pass - Must be run as administrator!
    write-host "Changing password...."
    try {
        #Invoke-VMScript -ScriptText $script -VM $server -GuestUser "administrator" -GuestPassword "wfsss" -ErrorAction Stop
        Invoke-VMScript -ScriptText $script -VM $server -GuestUser "administrator" -GuestPassword "$old_pass" -ErrorAction Stop
    } catch {
        write-host "Could not change password on $server!" -foregroundcolor "red"
        $ErrorMessage = $_.Exception.Message 
        write-host $ErrorMessage -foregroundcolor "red"
    }
    #Test new pass
    if (!($ErrorMessage)) {
        write-host "Testing the new password: $short_hostname$new_pass"
        try {
            Invoke-VMScript -ScriptText "write-host IT WORKS -foregroundcolor green" -VM $server -GuestUser $user -GuestPassword "$short_hostname$new_pass" -ErrorAction Stop
        } catch {
            write-host "New password doesn't work on $server" -foregroundcolor "red"
            write-host $_.Exception.Message -foregroundcolor "red"
        }
    } else {
        Remove-Variable ErrorMessage
    }
}
