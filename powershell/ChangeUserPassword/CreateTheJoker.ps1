######################
#Create local administrator using invoke-vmscript
#Author: CJ
######################

import-module VMware.VimAutomation.Core

Connect-VIServer vcenter09.virtualdatacenter.nu

#########
#Find all windows servers on vcenter
#Get-VM | where { $_.Guest.OSFullName -like "*Windows*" } | select-object Name, @{N="OSFullName";E={@($_.guest.OSFullName)}}
#########

#Define servers
$servers = @(
    "beta-live.sippeer.dk"
)

#Request for passwords
$admin_pass = Read-Host 'Enter the administrator password'
$new_user_pass = Read-Host 'Enter the new users password'

#Define the script creating the new user
$script = @'
NET USER thejoker #pass# /ADD
NET LOCALGROUP "administrators" "thejoker" /add
'@

$script = $script.Replace('#pass#', $new_user_pass)

#Create user on each server and test the user afterwards
foreach ($server in $servers) {
    write-host "Creating new user on $server"
    Invoke-VMScript -ScriptText $script -VM $server -GuestUser administrator -GuestPassword $admin_pass
    write-host "Testing new user on $server"
    Invoke-VMScript -ScriptText "write-host IT WORKS" -VM $server -GuestUser thejoker -GuestPassword $new_user_pass
}
