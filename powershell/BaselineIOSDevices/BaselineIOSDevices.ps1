﻿#Purpose: Baseline IOS devices
#Author: CJ

#Declare functions inside script block so we can send them to Start job later
$functions = {
    function WaitForSerialOutput
    {
        [CmdletBinding()]
        [Alias()]
        [OutputType([int])]
        Param (
            # Port
            [Parameter(Mandatory=$true,
                       ValueFromPipelineByPropertyName=$true,
                       Position=0)]
            [Object]
            $port,

            #The string we are Waiting for
            [Parameter(Mandatory=$true,
                       ValueFromPipelineByPropertyName=$true,
                       Position=1)]
            [string]
            $output,

            #Number of tries before giving up
            [Parameter(Mandatory=$true,
                       ValueFromPipelineByPropertyName=$true,
                       Position=2)]
            [int]
            $maxtries,

            #Wait interval for each try
            [Parameter(Mandatory=$true,
                       ValueFromPipelineByPropertyName=$true,
                       Position=3)]
            [int]
            $waitinterval
        )
        Begin {
            $promptRegEx = [regex]'[\$%#>] $'
            $x = 1
        }
        Process {
            do {
                write-host "Waiting for output: $output -" ($maxtries * $waitinterval - $x * $waitinterval) "seconds to timeout"
                $out = ''
                $port.WriteLine("")
                $port.Write("`r`n")
                $port.Write("`r`n")
                Start-Sleep $waitinterval
                $out += $port.ReadExisting()
                $outputLines = $out.Split("`n")
                foreach ($line in $outputLines) {
                    if ($line -notmatch $promptRegEx -and $line.length -gt 0) {
                        write-host $line
                    } elseif ($line -like "*baud-rate auto detection is enabled*") {
                        $port.Write("`r`n")
                        $port.Write("`r`n")
                    }
                }
                #write-host "X is: $x - Maxtries is: $maxtries"
                $x++
            }
            while ($out -notlike "*$output*" -and $x -le $maxtries)
        }
        End{
            if ($out -like "*$output*") {
                return "OK"
            } else {
                write-host "Device timeout - waited too long for: $output"
                return "Timeout"
            }
        }
    }

    function Invoke-SerialCommand
    {
        [CmdletBinding()]
        [Alias()]
        [OutputType([int])]
        Param (
            # Port
            [Parameter(Mandatory=$true,
                       ValueFromPipelineByPropertyName=$true,
                       Position=0)]
            [Object]
            $port,

            # Command to execute on device.
            [Parameter(Mandatory=$true,
                       ValueFromPipelineByPropertyName=$true,
                       Position=0)]
            [string]
            $command
        )

        Begin {
            $promptRegEx = [regex]'[\$%#>] $'
        }
        Process {

            $port.writeline($command)

            Start-Sleep -Milliseconds 500

            $out = ''

            # read all output until there is no more
            do {
                $out += $port.ReadExisting()

            } while ($port.BytesToRead -notlike 0)

            $outputLines = $out.Split("`n")
            foreach ($line in $outputLines) {
                if ($line -notmatch $promptRegEx) {
                    $line
                }
            }
        }
        End{}
    }

    function PrepareLogin
    {
        [CmdletBinding()]
        [Alias()]
        [OutputType([int])]
        Param (
            # Port
            [Parameter(Mandatory=$true,
                       ValueFromPipelineByPropertyName=$true,
                       Position=0)]
            [Object]
            $port,

            #Number of tries before giving up
            [Parameter(Mandatory=$true,
                       ValueFromPipelineByPropertyName=$true,
                       Position=1)]
            [int]
            $maxtries,

            #Wait interval for each try
            [Parameter(Mandatory=$true,
                       ValueFromPipelineByPropertyName=$true,
                       Position=2)]
            [int]
            $waitinterval
        )
        Begin {
            $x = 1
        }
        Process {
            while( $last -notlike "*User Name*" -and $x -le $maxtries ) {
                $out = ''
                $port.Write("`r`n")
                start-sleep -Milliseconds $waitinterval
                $out += $port.ReadExisting()
                $outputLines = $out.Split("`n")
                $last = $outputLines | Select -Last 1
            }
        }
        End{
            if ($last -like "*User Name*") {
                write-host "Ready to login: " $last
            } elseif ($x -ge $maxtries) {
                write-host "Maxtries reached - stopping now..."
            }
        }
    }
}

#Source functions inside this script
. $functions

#Test TFTP Server
write-host "Testing TFTP connection..."
function testtftp {
    tftp 192.168.2.2 get cisco_config.txt C:\temp\cisco_config.txt
}
try {
    $ErrorActionPreference = "Stop";
    testtftp
} catch {
    write-host "There is no connection to the tftp server on 192.168.2.2 - Please check TFTP server, IP configuration, etc."
    Read-Host "Press any key to exit..."
    exit
} finally {
   $ErrorActionPreference = "Continue"; #Reset the error action pref to default
}

#Ask for mail address
$mailto = Read-Host 'Which mail address should I send the result to? (If empty no mail will be sent)'

#Testing COMS
write-host "Testing COM ports..."
$COMS = [System.IO.Ports.SerialPort]::getportnames() | where { $_ -notlike "COM1" -and $_ -notlike "COM3" }
$COM_objects = @()
Foreach($COM in $COMS) {
    $COM_object = New-Object -TypeName PSObject
    $COM_object | Add-Member -MemberType NoteProperty –Name Port –Value $COM
    $COM_object | Add-Member -MemberType NoteProperty –Name IP –Value "N/A"
    $COM_object | Add-Member -MemberType NoteProperty –Name Connection –Value "N/A"
    $COM_object | Add-Member -MemberType NoteProperty –Name MAC –Value "N/A"
    $COM_object | Add-Member -MemberType NoteProperty –Name LogFile –Value "N/A"
    $err = $false
    Write-host "Testing $COM"
    $port = new-Object System.IO.Ports.SerialPort $COM,115200,None,8,1
    $x = 1
    while (!($port.IsOpen) -and $x -le 5) {
        try {
            $port.open()
        } catch {
            $err = $true
        }
        if ($err) {
            $x++
            write-host "Error opening port for $COM - Trying a few times"
            sleep 5
        }
    }
    if (!($err)) {
        Write-host "Waiting 12 sec for port to respond"
        do {
            $port.Write("`r`n")
            $port.Write("`r`n")
            Start-Sleep 4
            $port.Write("`r`n")
            $port.Write("`r`n")
            $port.WriteLine("")
            Start-Sleep 4
            $port.Write("`r`n")
            $port.Write("`r`n")
            Start-Sleep 4
            break
        } while ($port.BytesToRead -like 0)
        if ($port.BytesToRead -notlike 0) {
            write-host "Got response from $COM... Waiting for Username prompt"
            #Check router is ready for login
            $con = WaitForSerialOutput -Port $port -output "User Name" -maxtries 50 -waitinterval 10
            $COM_object.Connection = $con
        } else {
            write-host "No response from $COM"
            $COM_object.Connection = "No response"
        }
        try {
            $port.close()
        } catch {}
    } else {
        write-host "Could not open port on $COM"
        $COM_object.Connection = "Could not open port"
    }
    $COM_objects += $COM_object
}

#Assign unique IP addresses
write-host "Assign IP addresses to interface vlan200..."
Foreach ($COM_object in $COM_objects) {
     if ($COM_object.Connection -like "OK") {
        $correct = "n"
        do {
            write-host ""
            $IP = Read-Host "Assign IP for" $COM_object.Port
        } while (($correct = Read-Host "Are you sure the IP is correct? (y/n)") -ne "y")
        $COM_object.IP = "$IP"
     }
}

write $COM_objects

#Async code
$script = {
    param($COM_object,$logpath)
    Start-Transcript -path $logpath
    $port = $COM_object.Port
    $IP = $COM_object.IP
    Write-Host "Starting job for: $port with IP: $IP"
    $port = new-Object System.IO.Ports.SerialPort $COM_object.Port,115200,None,8,1
    $port.open()
    PrepareLogin -port $port -maxtries 100 -waitinterval 300

    #Username
    Invoke-SerialCommand -port $port -command "cisco"

    #Password
    Invoke-SerialCommand -port $port -command "cisco"

    #Say no to change password from default settings
    Invoke-SerialCommand -port $port -command "N"

    #Check we are ready to configure device
    $port.WriteLine("show mac address-table")
    start-sleep 1
    $mactable = $port.ReadExisting()
    write-host "Here is the mac table: " $mactable

    $ownmac = $mactable | findstr "self"

    write-host "here is own mac: " $ownmac

    #Check we are ready to configure device
    $macaddr = ($ownmac | select-string -pattern "..:..:..:..:..:..")[0].Matches.Value
    if ($macaddr) {

        write-host "This device has the following MAC address: " $macaddr

        #Waiting for connection to default gateway 192.168.2.1
        $y = 1
        while ($port.ReadExisting() -notlike "*bytes from*" -and $y -le 20) {
            $port.WriteLine("ping 192.168.2.1")
            write-host "Waiting for connection to default gateway 192.168.2.1..."
            start-sleep 6
            $y++
        }

        #Select boot image
        #write-host "Selecting boot image..."
        #$port.WriteLine("")
        #$bootvar = Invoke-SerialCommand -port $port -command "show bootvar"
        #$bootvar = $bootvar -split "`n"
        #$image1 = ($bootvar | select-string -pattern "image-")[0]
        #$image2 = ($bootvar | select-string -pattern "image-")[1]
        #if ($image1 -like "*Not active*") {
        #    $targetimage = "image-1"
        #} else {
        #    $targetimage = "image-2"
        #}
        #For this command success is "The copy operation was completed successfully"
        #write-host "Starting to copy new image from tftp..."
        #Invoke-SerialCommand -port $port -command "copy tftp://192.168.2.2/sx300_fw-1486.ros $targetimage"
        #WaitForSerialOutput -port $port -output "bytes copied in" -maxtries 50 -waitinterval 10
        #Invoke-SerialCommand -port $port -command "boot system $targetimage"

        #Configure router mode and restart
        #write-host "Configuring system mode router..."
        #Invoke-SerialCommand -port $port -command "set system mode router"
        #Invoke-SerialCommand -port $port -command "Y"

        #Router restarts
        #write-host "Waiting for router to restart..."
        #WaitForSerialOutput -port $port -output "User Name" -maxtries 80 -waitinterval 10

        #Username password (Cant login again because of Console baud-rate auto detection is enabled - we have to change the speed to 9600)
        #$port.close()
        #$port = new-Object System.IO.Ports.SerialPort $COM_object.Port,9600,None,8,1
        #$port.open()
        #PrepareLogin -port $port -maxtries 100 -waitinterval 300
        #$port.WriteLine("")
        #Invoke-SerialCommand -port $port -command "cisco"
        #Invoke-SerialCommand -port $port -command "cisco"
        #Invoke-SerialCommand -port $port -command "N"

        #Waiting for connection to default gateway 192.168.2.1
        #$y = 1
        #while ($port.ReadExisting() -notlike "*bytes from*" -and $y -le 20) {
        #    $port.WriteLine("ping 192.168.2.1")
        #    write-host "Waiting for connection to default gateway 192.168.2.1..."
        #    start-sleep 6
        #    $y++
        #}

        #Prepare new config file
        write-host "Starting to copy new configuration file from tftp..."
        Invoke-SerialCommand -port $port -command "copy tftp://192.168.2.2/cisco_config.txt startup"
        #Invoke-SerialCommand -port $port -command "Y"
        $port.Write("Y")

        WaitForSerialOutput -port $port -output "The copy operation was completed successfully" -maxtries 20 -waitinterval 10

        #Router restarts
        write-host "Restarting router..."
        $port.WriteLine("reload")
        $port.Write("Y")
        $port.Write("Y")
        write-host "Waiting for router to restart..."
        WaitForSerialOutput -port $port -output "User Name" -maxtries 80 -waitinterval 10
        #$port.close()
        #$port = new-Object System.IO.Ports.SerialPort $COM_object.Port,9600,None,8,1
        #$port.open()
        PrepareLogin -port $port -maxtries 100 -waitinterval 300
        #$port.WriteLine("")

        #Username password
        Invoke-SerialCommand -port $port -command "admin"
        Invoke-SerialCommand -port $port -command "f8406!"

        #Set vlan200 address
        write-host "Configuring interface vlan200 with unique IP: $IP"
        Invoke-SerialCommand -port $port -command "conf t"
        Invoke-SerialCommand -port $port -command "interface vlan 200"
        Invoke-SerialCommand -port $port -command "name VOIP"
        Invoke-SerialCommand -port $port -command "ip address $IP 255.255.255.0"
        Invoke-SerialCommand -port $port -command "ip dhcp relay enable"
        Invoke-SerialCommand -port $port -command "end"
        Invoke-SerialCommand -port $port -command "wr mem"
        Invoke-SerialCommand -port $port -command "Y"

        #Show startup config
        write-host "Show startup configuration..."
        $port.WriteLine("sh startup-config")
        $port.WriteLine("<spa")
        $port.WriteLine("<spa")
        $port.WriteLine("<spa")
        $port.WriteLine("<spa")
        $port.WriteLine("<spa")
        start-sleep 10
        $startconfig = $port.ReadExisting()
        write-host $startconfig

        #Show ip interfaces
        write-host "Show ip interfaces configuration..."
        $port.WriteLine("sh ip int")
        start-sleep 5
        $ipinterfaces = $port.ReadExisting()
        write-host $ipinterfaces

        write-host "Done!"

    } else {
        #Problems getting MAC address - perhaps not logged in properly
        write-host "Could not get MAC for " $COM_object.Port " - perhaps not logged in properly"
    }

    $port.close()
    Stop-Transcript
}

#Start Async jobs
$datestring = $(get-date -f ddMMyyyyHHmm)
write-host "Starting async jobs..."
ForEach ($COM_object in $COM_objects) {
    if ($COM_object.Connection -like "OK") {
        $name = $COM_object.Port
        $logpath = join-path "C:\temp" $("Baseline_device_" + $name + "_" + $datestring + ".log")
        $COM_object.LogFile = "$logpath"
        Start-Job -Name $name -InitializationScript $functions -ScriptBlock $script -ArgumentList $COM_object,$logpath | out-null
    }
}

#Cleanup logs
#Remove-Item C:\temp\Baseline_device_COM* -Force

#Open log file monitors
write-host "Opening log file monitors..."
ForEach ($COM_object in $COM_objects) {
    if ($COM_object.Connection -like "OK") {
        $name = $COM_object.Port
        $logpath = $COM_object.LogFile
        start-process powershell.exe -argument "`$host.ui.RawUI.WindowTitle = '$name'; get-content -Path $logpath -wait"
    }
}

#Monitor async jobs
write-host "Monitoring async jobs..."
$y = 1
While ((Get-Job -State "Running") -and ($y -le 180)) {
    write-host ""
    write-host "Status of background jobs - Refreshing every 20 sec - Maximum runtime is 1 hour."
    write-host ""
    $jobs = Get-Job | select ID,NAME,State
    foreach($job in $jobs){
        $name = $job.Name
        #$logpath = join-path "C:\temp" $("Baseline_device_" + $name + ".log")
        #$job | Add-Member -MemberType NoteProperty –Name LogFile –Value "$logpath"
        #Receive-Job -Name $name 2>&1 >> $logpath
    }
    $jobs | Format-Table
    Start-Sleep 20
    $y++
}

write-host "Jobs completed, getting output. All log files is placed under C:\temp"

write-host "Removing jobs from powershell..."
Stop-Job *
Remove-Job *

#Find MAC addresses
write-host "Trying to find MAC address on each COM device..."
ForEach ($COM_object in $COM_objects) {
    if ($COM_object.Connection -like "OK") {
        $line1 = Get-Content $COM_object.LogFile | Select-String -Pattern "This device has the following MAC address" -SimpleMatch | select-object -First 1
        if ($line1) {
            $MAC = ($line1.ToString() | select-string -pattern "..:..:..:..:..:..")[0].Matches.Value
            $COM_object.MAC = $MAC
        } else {
            $MAC = "No MAC found - maybe an error occurred"
        }
    }
}

#Build final COM_object using output from logs
write-host "Building final mail object..."
$date = date
$fragment = $COM_objects | ConvertTo-HTML -fragment
$body = @"
<font color=""black"" face=""Arial"" size=""3"">
<style type=""text/css"">body{font: .8em ""Lucida Grande"", Tahoma, Arial, Helvetica, sans-serif;}
table {
    width: 1200px;
    border-collapse: collapse;
    margin:50px auto;
    }
/* Zebra striping */
tr:nth-of-type(odd) {
    background: #eee;
    }
th {
    background: #349AB5;
    color: white;
    font-weight: bold;
    }
td, th {
    padding: 6px;
    border: 1px solid #ccc;
    text-align: left;
    font-size: 16px;
    }
</style>
<body BGCOLOR=""white"">
    <div align="center">
        <h1 style='font-family:arial;'><b>Baseline IOS Devices</b></h1>
        <p>Report generated: $date</p>
        $fragment
        <p>Please check the end of the logfiles to make sure that all devices are correctly configured</p>
    </div>
</body>
"@

#When all jobs are done - send mail with COM Objects
if ($mailto) {
    write-host "Sending mail..."
    send-mailmessage -from "lager-pc@flexfone.dk" -to $mailto -subject "Baseline IOS Devices" -body $body -BodyAsHtml -Attachment @(Get-Item C:\temp\* | where { $_.Name -like "Baseline_device_COM*_$datestring.log" }) -smtpServer smtp1.sippeer.dk
}

$body | Out-File "C:\temp\BaselineIOSDevices.html"

write-host "Done - press enter to exit"
pause
