﻿Connect-VIServer vcenter09.virtualdatacenter.nu

$vms = Get-VM | Where-Object {$_.PowerState -like "PoweredOn"}
$allvms = @()

foreach($vm in $vms){
    $vmstat = "" | Select VmName, MemAssignedMB, MemMax, MemAvg, MemMin, MemMaxUsedMB, MemMaxUnder70Percent, SuggestedMemMB, DifferenceInMB
    $vmstat.VmName = $vm.name
    #$statcpu = Get-Stat -Entity ($vm)-start (get-date).AddDays(-30) -Finish (Get-Date)-MaxSamples 10000 -stat cpu.usage.average
    $statmem = Get-Stat -Entity ($vm)-start (get-date).AddDays(-7) -Finish (Get-Date)-MaxSamples 10000 -stat mem.usage.average
    #$cpu = $statcpu | Measure-Object -Property value -Average -Maximum -Minimum
    $mem = $statmem | Measure-Object -Property value -Average -Maximum -Minimum

    #$vmstat.CPUMax = $cpu.Maximum
    #$vmstat.CPUAvg = $cpu.Average
    #$vmstat.CPUMin = $cpu.Minimum
    $vmstat.MemMax = $mem.Maximum
    $vmstat.MemAvg = $mem.Average
    $vmstat.MemMin = $mem.Minimum
    $vmstat.MemAssignedMB = $vm.MemoryGB * 1024
    $vmstat.MemMaxUsedMB = $vmstat.MemAssignedMB * ($vmstat.MemMax / 100)
    if ($vmstat.MemMax -lt 70) {
        $vmstat.MemMaxUnder70Percent = "true"
    } else {
        $vmstat.MemMaxUnder70Percent = "false"
    }
    $Needed = $vmstat.MemMaxUsedMB * 1.30
    $vmstat.SuggestedMemMB = [math]::Round(($Needed),2) 
    $vmstat.DifferenceInMB = $vmstat.MemAssignedMB - $vmstat.SuggestedMemMB
    $allvms += $vmstat
}

$allvms | Select VmName, MemAssignedMB, MemMax, MemAvg, MemMin, MemMaxUsedMB, MemMaxUnder70Percent, SuggestedMemMB, DifferenceInMB | sort $_.DifferenceInMB | Out-GridView