#########################################################
#                                                       #
# Monitoring Windows Updates and Pending Restarts       #
#                                                       #
#########################################################
$hostname = hostname

$WUSettings = (New-Object -com "Microsoft.Update.AutoUpdate").Settings
$WUSettings.NotificationLevel=1
$WUSettings.save() 

#########################################################
# List of users who will receive the report
#########################################################
$mailto = "cj@flexfone.dk, drift@flexfone.dk"
#$mailto = "cj@flexfone.dk"

#########################################################
# SMTP properties
#########################################################
$emailFrom = "$hostname@flexfone.dk"
$smtpServer = "smtp1.sippeer.dk" #SMTP Server.
$smtpUsername = ""
$smtpPassword = ""

#########################################################
# Collecting WU information
#########################################################
$service = get-service wuauserv
$WUStatus = $service.Status

$UpdateSession = New-Object -ComObject Microsoft.Update.Session
$UpdateSearcher = $UpdateSession.CreateUpdateSearcher()
$SearchResult = $UpdateSearcher.Search("IsAssigned=1 and IsHidden=0 and IsInstalled=0")
$Critical = $SearchResult.updates | where { $_.MsrcSeverity -eq "Critical" }
$important = $SearchResult.updates | where { $_.MsrcSeverity -eq "Important" }
$other = $SearchResult.updates | where { $_.MsrcSeverity -eq $null }
# Get windows updates counters
$totalUpdates = $($SearchResult.updates.count)
$totalCriticalUp = $($Critical.count)
$totalImportantUp = $($Important.count)

if($totalUpdates -gt 0) {
    $updatesToInstall = $true
} else { 
    $updatesToInstall = $false 
}

#########################################################
# Checking pending reboot
#########################################################
function Test-PendingReboot
{
 if (Get-ChildItem "HKLM:\Software\Microsoft\Windows\CurrentVersion\Component Based Servicing\RebootPending" -EA Ignore) { return $true }
 if (Get-Item "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\WindowsUpdate\Auto Update\RebootRequired" -EA Ignore) { return $true }
 if (Get-ItemProperty "HKLM:\SYSTEM\CurrentControlSet\Control\Session Manager" -Name PendingFileRenameOperations -EA Ignore) { return $true }
 try { 
   $util = [wmiclass]"\\.\root\ccm\clientsdk:CCM_ClientUtilities"
   $status = $util.DetermineIfRebootPending()
   if(($status -ne $null) -and $status.RebootPending){
     return $true
   }
 }catch{}
 
 return $false
}

if (Test-PendingReboot) {
    $machineNeedsRestart = $true
} else {
    $machineNeedsRestart = $false
}

#########################################################
# Creating mail content 
#########################################################
if($machineNeedsRestart -or $updatesToInstall -or ($WUStartType -eq "Manual")) {
    $mail_content = New-Object PSObject -Property @{
        Computer = $hostname 
        WindowsUpdateStatus = $WUStatus 
        UpdatesToInstall = $updatesToInstall 
        TotalOfUpdates = $totalUpdates  
        TotalOfCriticalUpdates = $totalCriticalUp 
        TotalOfImportantUpdates = $totalImportantUp
        RebootPending = $machineNeedsRestart
    }
    $tableFragment2 = ""
    Foreach ($update in $SearchResult.updates) {
        $tableFragment2 += "<p>" + $update.Title + "</p>"
    }
}

#########################################################
# Formating result
#########################################################
$tableFragment = $mail_content | ConvertTo-HTML -fragment

# HTML Format for Output 
$HTMLmessage = @"
<font color=""black"" face=""Arial"" size=""3"">
<h1 style='font-family:arial;'><b>Windows Updates and Pending Restarts Report</b></h1>
<br><br>
<style type=""text/css"">body{font: .8em ""Lucida Grande"", Tahoma, Arial, Helvetica, sans-serif;}
ol{margin:0;}
table{width:80%;text-align:center;}
thead{}
thead th{font-size:120%;}
th{border-bottom:2px solid rgb(79,129,189);border-top:2px solid rgb(79,129,189);padding-bottom:10px;padding-top:10px;}
tr{padding:10px 10px 10px 10px;border:none;}
#middle{background-color:#900;}
</style>
<body BGCOLOR=""white"">
$tableFragment
<br><br>
<h3>List of update titles</h3>
$tableFragment2
<br><p><b>This script is running on rdshost.sippeer.dk</b></p>
</body>
"@

#########################################################
# Validation and sending email
#########################################################
# Regular expression to get what's inside of <td>'s
$regexsubject = $HTMLmessage
$regex = [regex] '(?im)<td>'

# If you have data between <td>'s then you need to send the email
if ($regex.IsMatch($regexsubject)) {
     $smtp = New-Object Net.Mail.SmtpClient -ArgumentList $smtpServer 
      $smtp.credentials = New-Object System.Net.NetworkCredential($smtpUsername, $smtpPassword); 
      $msg = New-Object Net.Mail.MailMessage
      $msg.From = $emailFrom
      $msg.To.Add($mailto)
      $msg.Subject = "Pending updates or restart for $hostname"
      $msg.IsBodyHTML = $true
      $msg.Body = $HTMLmessage    
      $smtp.Send($msg)   
}