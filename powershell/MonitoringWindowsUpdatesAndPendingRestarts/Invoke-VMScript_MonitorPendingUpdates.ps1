﻿import-module VMware.VimAutomation.Core

$vm_creds_file = "C:\temp\vm_creds.clixml"
$guest_creds_file = "C:\temp\guest_creds.clixml"

$logfile = "C:\temp\Invoke-VMScript_MonitorPendingUpdates.log"

####Generate logins for vmware and guests - only run this part once to generate credential-files####
#write-host Vmware Credentials:
#$vm_creds = Get-Credential
#$vm_creds | Export-CLiXml $vm_creds_file
#write-host Guest Credentials:
#$guest_creds = Get-Credential
#$guest_creds | Export-CLiXml $guest_creds_file
####

Start-Transcript -path $logfile

####Import the stored credentials - run this part everytime to import the stored credentials####
$vm_creds = Import-CliXml $vm_creds_file
$guest_creds = Import-CliXml $guest_creds_file
####

Connect-VIServer vcenter09.virtualdatacenter.nu -Credential $vm_creds

$servers = @(
    "beta-live.sippeer.dk"
)

$source = "C:\scripts\flexfone_scripts\powershell\MonitoringWindowsUpdatesAndPendingRestarts\MonitoringWindowsUpdatesAndPendingRestarts.ps1"
$destination = "C:\tmp\MonitoringWindowsUpdatesAndPendingRestarts.ps1"

foreach ($server in $servers) {
    write-host "Running script at: $server"
    Copy-VMGuestFile -LocalToGuest -VM $server -Source "$source" -Destination "$destination" -GuestCredential $guest_creds -Verbose -Force
    Invoke-VMScript -ScriptText "C:\tmp\MonitoringWindowsUpdatesAndPendingRestarts.ps1" -VM $server -GuestCredential $guest_creds
}

Stop-Transcript