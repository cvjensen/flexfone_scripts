Connect-VIServer vcenter09.virtualdatacenter.nu

$servers = @(
"beta-live.sippeer.dk"
"iis01.test.crm.sippeer.dk"
"dist01.sippeer.dk"
"liveproxy04.sippeer.dk"
"public01.sippeer.dk"
"service01.sippeer.dk"
"dist02.sippeer.dk"
"dist03.sippeer.dk"
"dist04.sippeer.dk"
"dist05.sippeer.dk"
"dist06.sippeer.dk"
"dist07.sippeer.dk"
"dist08.sippeer.dk"
"dist09.sippeer.dk"
"dist10.sippeer.dk"
"dist11.sippeer.dk"
"dist12.sippeer.dk"
"dist13.sippeer.dk"
"dist14.sippeer.dk"
"dist15.sippeer.dk"
"dist16.sippeer.dk"
"dist17.sippeer.dk"
"dist18.sippeer.dk"
"dist19.sippeer.dk"
"dist20.sippeer.dk"
"esb01crm.sippeer.dk"
"esb02crm.sippeer.dk"
"exchsync01.sippeer.dk"
"iis01.crm.sippeer.dk"
"iis01.sippeer.dk"
"iis02.crm.sippeer.dk"
"liveproxy01.sippeer.dk"
"liveproxy02.sippeer.dk"
"liveproxy03.sippeer.dk"
"stat02.sippeer.dk"
"stat03.sippeer.dk"
)

foreach ($server in $servers) {
    Invoke-VMScript -ScriptText "hostname; set-executionpolicy unrestricted" -VM $server -GuestUser administrator -GuestPassword wf
}
