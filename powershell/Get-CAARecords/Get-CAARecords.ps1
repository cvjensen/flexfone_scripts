﻿<#
.DESCRIPTION
   Checks domain for CAA records.
.EXAMPLE
   Get-CAARecords -domain "google.com"
#>
function Get-CAARecords
{
    [CmdletBinding()]
    [Alias()]
    [OutputType([int])]
        Param (
        # Port
        [Parameter(Mandatory=$true,
            ValueFromPipelineByPropertyName=$true,
            Position=0)]
            [Object]
            $domain
        )
        Begin {
            $url = "https://www.digwebinterface.com/?hostnames=$domain&type=CAA&ns=resolver&useresolver=8.8.4.4&nameservers="
        }
        Process {
            $html = Invoke-WebRequest -Uri "$url"
            $response = $html.AllElements | Where tagName -eq "pre" | select -ExpandProperty innerTEXT
            $object = New-Object -TypeName PSObject
            $object | Add-Member -MemberType NoteProperty –Name CAA –Value $false
            $object | Add-Member -MemberType NoteProperty –Name LetsEncrypt –Value $false
            $object | Add-Member -MemberType NoteProperty –Name Response –Value $response
        }
        End{
            if ($response -like "*CAA*") {
                $object.CAA = $true
                if ($response -like "*letsencrypt*") {
                    $object.LetsEncrypt = $true
                }
            }
            return $object
        }
}

