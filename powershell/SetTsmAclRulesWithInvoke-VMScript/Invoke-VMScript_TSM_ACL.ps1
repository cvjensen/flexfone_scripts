#Running Invoke-VMScript on multiple servers.
#Remember to run connect-viserver before running the script
#Author: CJ

$servers = @(
"dist01.sippeer.dk", 
"dist02.sippeer.dk"
)

$script = @'
$key = "HKLM:\SOFTWARE\IBM\ADSM\CurrentVersion\Nodes"
$acl = Get-ACL -Path $key
$acl.SetAccessRuleProtection($true, $false)
$rule = New-Object System.Security.AccessControl.RegistryAccessRule(((New-Object System.Security.Principal.SecurityIdentifier([System.Security.Principal.WellKnownSidType]::BuiltinAdministratorsSid, $null)).Translate([System.Security.Principal.NTAccount]).Value), "FullControl", "ContainerInherit", "None", "Allow")
$acl.AddAccessRule($rule)
$rule = New-Object System.Security.AccessControl.RegistryAccessRule(((New-Object System.Security.Principal.SecurityIdentifier([System.Security.Principal.WellKnownSidType]::LocalSystemSid, $null)).Translate([System.Security.Principal.NTAccount]).Value), "FullControl", "ContainerInherit", "None", "Allow")
$acl.AddAccessRule($rule)
Set-Acl -Path $key -AclObject $acl
get-acl -path $key | format-list
'@

$pass = read-host "Guest pass: " -assecurestring

foreach ($server in $servers) {
    write-host "Running script at: $server"
    Invoke-VMScript -ScriptText $script -VM $server -GuestUser administrator -GuestPassword $pass
}
