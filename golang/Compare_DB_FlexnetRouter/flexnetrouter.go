package main

import "bufio"
import "os"
import "regexp"

func (arp_box *ArpEntryBox) GetShowArp(path string) {
    ipRegex := regexp.MustCompile("\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}")
    macRegex := regexp.MustCompile("....\\.....\\.....")
    intRegex := regexp.MustCompile("./././.{7,11}")
    file, err := os.Open(path)
    checkErr(err)
    defer file.Close()
    scanner := bufio.NewScanner(file)
    for scanner.Scan() {
        text := scanner.Text()
        ipMatch := ipRegex.FindStringSubmatch(text)
        macMatch := macRegex.FindStringSubmatch(text)
        intMatch := intRegex.FindStringSubmatch(text)
        if len(intMatch) > 0 {
          a_arp := ArpEntry{Ip: ipMatch[0], Mac: macMatch[0], Int: intMatch[0]}
          arp_box.Items = append(arp_box.Items, a_arp)
        }
    }
}

func (dhcp_box *DhcpEntryBox) GetShowDhcp(path string) {
    ipRegex := regexp.MustCompile("\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}")
    macRegex := regexp.MustCompile("....\\.....\\.....")
    intRegex := regexp.MustCompile("./././.\\..........")
    vrfRegex := regexp.MustCompile("default|VOIP")
    file, err := os.Open(path)
    checkErr(err)
    defer file.Close()
    scanner := bufio.NewScanner(file)
    for scanner.Scan() {
        text := scanner.Text()
        ipMatch := ipRegex.FindStringSubmatch(text)
        macMatch := macRegex.FindStringSubmatch(text)
        intMatch := intRegex.FindStringSubmatch(text)
        vrfMatch := vrfRegex.FindStringSubmatch(text)
        if len(intMatch) > 0 {
          a_dhcp := DhcpEntry{Ip: ipMatch[0], Mac: macMatch[0], Int: intMatch[0], Vrf: vrfMatch[0]}
          dhcp_box.Items = append(dhcp_box.Items, a_dhcp)
        }
    }
}

func (static_route_box *StaticRouteEntryBox) GetShowRouteVrf(path string) {
    staticRegex := regexp.MustCompile("^S")
    ipRegex := regexp.MustCompile("\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}")
    file, err := os.Open(path)
    checkErr(err)
    defer file.Close()
    scanner := bufio.NewScanner(file)
    for scanner.Scan() {
        text := scanner.Text()
        staticMatch := staticRegex.FindStringSubmatch(text)
        if len(staticMatch) > 0 {
          ipMatch := ipRegex.FindAllStringSubmatch(text, -1)
          a_static_route := StaticRouteEntry{Ip1: ipMatch[0][0], Ip2: ipMatch[1][0]}
          static_route_box.Items = append(static_route_box.Items, a_static_route)
        }
    }
}
