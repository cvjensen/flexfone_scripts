package main

import (
	"net/smtp"
)

func SendError(body string) (error) {
	from := "flexnet01@flexfone.dk"
	to := "cj@flexfone.dk"
  server := "smtp1.sippeer.dk:25"
  mime := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n";
  subject := "Subject: Error when comparing Flexnet DB/Router!\n"
  msg := []byte(subject + mime + body)
  err := smtp.SendMail(server, smtp.PlainAuth("", from, "", server), from, []string{to}, msg)
  if err != nil {
    return err
  }
  return nil
}
