package main

import "database/sql"
import _ "github.com/go-sql-driver/mysql"
import "log"

func GetErrors(db *sql.DB) (int) {
  error_count := 0

  //Getting Kredsloeb with more than 1 local IP
  rows, err := db.Query(`SELECT * FROM drift.FlexnetGetLocalIpCount;`)
  checkErr(err)
  defer rows.Close()
  for rows.Next() {
      error_count++
      var Kredsloebsnummer string
      var Count string
      err := rows.Scan(&Kredsloebsnummer, &Count)
      checkErr(err)
      log.Printf("Kredsloeb: %v has %v local IP addresses\n", Kredsloebsnummer, Count)
  }

  //Getting Ip's with more than one entry in DHCP
  rows, err = db.Query(`SELECT * FROM drift.FlexnetGetDhcpCount;`)
  defer rows.Close()
  checkErr(err)
  for rows.Next() {
      error_count++
      var Kredsloebsnummer string
      var Ip string
      var Count string
      err := rows.Scan(&Kredsloebsnummer, &Ip, &Count)
      checkErr(err)
      log.Printf("Kredsloeb: %v with IP: %v has %v DHCP entries\n", Kredsloebsnummer, Ip, Count)
  }
  return error_count
}
