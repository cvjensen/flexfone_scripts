package main

import "database/sql"
import _ "github.com/go-sql-driver/mysql"

type row struct {
    Kredsloebsnummer      sql.NullString
    External_IP           sql.NullString
    VOIP_IP               sql.NullString
    Local_IP              sql.NullString
    status                sql.NullInt64
}

type l_row struct {
    Loesninger_pbx_id      sql.NullString
    Kredsloebsnummer       sql.NullString
    Aktiv                  sql.NullString
}

type Kredsloeb struct {
    Kredsloebsnummer      string
    External_IP           string
    VOIP_IP               string
    Local_IP              string
    Status                string
}

type KredsloebBox struct {
    Items []Kredsloeb
}

type Loesning struct {
    Loesninger_pbx_id      string
    Kredsloebsnummer       string
    Aktiv                  string
}

type LoesningBox struct {
    Items []Loesning
}

type ArpEntry struct {
    Ip  string
    Mac string
    Int string
}

type ArpEntryBox struct {
    Items []ArpEntry
}

type DhcpEntry struct {
    Ip  string
    Mac string
    Int string
    Vrf string
}

type DhcpEntryBox struct {
    Items []DhcpEntry
}

type StaticRouteEntry struct {
    Ip1   string
    Ip2   string
}

type StaticRouteEntryBox struct {
    Items []StaticRouteEntry
}
