package main

import "strings"
import "database/sql"
import _ "github.com/go-sql-driver/mysql"

func exportKredsloeb(k_box KredsloebBox, db *sql.DB) {
  _, err := db.Exec("truncate flexnet_kredsloeb")
  checkErr(err)
  sqlStr := "INSERT INTO flexnet_kredsloeb(Kredsloebsnummer, External_IP, VOIP_IP, Local_IP, Status) VALUES "
  vals := []interface{}{}
  for _, row := range k_box.Items {
      sqlStr += "(?, ?, ?, ?, ?),"
      vals = append(vals, row.Kredsloebsnummer, row.External_IP, row.VOIP_IP, row.Local_IP, row.Status)
  }
  //trim the last ,
  sqlStr = strings.TrimSuffix(sqlStr, ",")
  //prepare the statement
  stmt, _ := db.Prepare(sqlStr)
  //format all vals at once
  _, err = stmt.Exec(vals...)
  checkErr(err)
}

func exportLoesning(l_box LoesningBox, db *sql.DB) {
  _, err := db.Exec("truncate flexnet_loesning")
  checkErr(err)
  sqlStr := "INSERT INTO flexnet_loesning(Loesninger_pbx_id, Kredsloebsnummer, Aktiv) VALUES "
  vals := []interface{}{}
  for _, row := range l_box.Items {
      sqlStr += "(?, ?, ?),"
      vals = append(vals, row.Loesninger_pbx_id, row.Kredsloebsnummer, row.Aktiv)
  }
  //trim the last ,
  sqlStr = strings.TrimSuffix(sqlStr, ",")
  //prepare the statement
  stmt, _ := db.Prepare(sqlStr)
  //format all vals at once
  //res, _ := stmt.Exec(vals...)
  _, err = stmt.Exec(vals...)
  checkErr(err)
}

func exportArpEntries(arp_box ArpEntryBox, db *sql.DB) {
  _, err := db.Exec("truncate flexnet_arp")
  checkErr(err)
  sqlStr := "INSERT INTO flexnet_arp(Ip, Mac, Interface) VALUES "
  vals := []interface{}{}
  for _, row := range arp_box.Items {
      sqlStr += "(?, ?, ?),"
      vals = append(vals, row.Ip, row.Mac, row.Int)
  }
  //trim the last ,
  sqlStr = strings.TrimSuffix(sqlStr, ",")
  //prepare the statement
  stmt, _ := db.Prepare(sqlStr)
  //format all vals at once
  //res, _ := stmt.Exec(vals...)
  _, err = stmt.Exec(vals...)
  checkErr(err)
}

func exportArpVrfEntries(arp_vrf_box ArpEntryBox, db *sql.DB) {
  _, err := db.Exec("truncate flexnet_arp_vrf")
  checkErr(err)
  sqlStr := "INSERT INTO flexnet_arp_vrf(Ip, Mac, Interface) VALUES "
  vals := []interface{}{}
  //fmt.Println(k_box)
  for _, row := range arp_vrf_box.Items {
      sqlStr += "(?, ?, ?),"
      vals = append(vals, row.Ip, row.Mac, row.Int)
  }
  //trim the last ,
  sqlStr = strings.TrimSuffix(sqlStr, ",")
  //prepare the statement
  stmt, _ := db.Prepare(sqlStr)
  //format all vals at once
  //res, _ := stmt.Exec(vals...)
  _, err = stmt.Exec(vals...)
  checkErr(err)
}

func exportDhcp(dhcp_box DhcpEntryBox, db *sql.DB) {
  _, err := db.Exec("truncate flexnet_dhcp")
  checkErr(err)
  sqlStr := "INSERT INTO flexnet_dhcp(Ip, Mac, Interface, Vrf) VALUES "
  vals := []interface{}{}
  for _, row := range dhcp_box.Items {
      sqlStr += "(?, ?, ?, ?),"
      vals = append(vals, row.Ip, row.Mac, row.Int, row.Vrf)
  }
  //trim the last ,
  sqlStr = strings.TrimSuffix(sqlStr, ",")
  //prepare the statement
  stmt, _ := db.Prepare(sqlStr)
  //format all vals at once
  //res, _ := stmt.Exec(vals...)
  _, err = stmt.Exec(vals...)
  checkErr(err)
}

func exportStaticRoutes(static_route_box StaticRouteEntryBox, db *sql.DB) {
  _, err := db.Exec("truncate flexnet_staticroutes")
  checkErr(err)
  sqlStr := "INSERT INTO flexnet_staticroutes(Ip1, Ip2) VALUES "
  vals := []interface{}{}
  for _, row := range static_route_box.Items {
      sqlStr += "(?, ?),"
      vals = append(vals, row.Ip1, row.Ip2)
  }
  //trim the last ,
  sqlStr = strings.TrimSuffix(sqlStr, ",")
  //prepare the statement
  stmt, _ := db.Prepare(sqlStr)
  //format all vals at once
  //res, _ := stmt.Exec(vals...)
  _, err = stmt.Exec(vals...)
  checkErr(err)
}
