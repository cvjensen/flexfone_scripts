package main

import "database/sql"
import _ "github.com/go-sql-driver/mysql"
import "strconv"

func (k_box *KredsloebBox) GetKredsloeb(flexnet_db *sql.DB) {
  rows, err := flexnet_db.Query(`SELECT distinct pce_ip.ip as External_IP,
  TDCinfo.Kredsloebsnummer,
  pcv_ip.voip_ip as VOIP_IP,
  pcl_ip.ip as Local_IP,
  pce_ip.status
  FROM provision_corenet_TDCinfo TDCinfo
  INNER JOIN provision_corenet_external_ip pce_ip ON pce_ip.kredsloebsnummer=TDCinfo.Kredsloebsnummer
  INNER JOIN provision_corenet_voip_ip pcv_ip ON pcv_ip.kredsloebsnummer=TDCinfo.Kredsloebsnummer
  LEFT JOIN provision_corenet_local_ip pcl_ip ON pcl_ip.fk_provision_corenet_external_ip_id=pce_ip.id;`)
  defer rows.Close()
  checkErr(err)
  for rows.Next() {
      var r row
      var k Kredsloeb
      err := rows.Scan(&r.External_IP, &r.Kredsloebsnummer, &r.VOIP_IP, &r.Local_IP, &r.status)
      checkErr(err)
      if (r.Kredsloebsnummer.Valid) {
          k.Kredsloebsnummer = r.Kredsloebsnummer.String
      } else {
          k.Kredsloebsnummer = "Empty"
      }
      if (r.External_IP.Valid) {
          k.External_IP = r.External_IP.String
      } else {
          k.External_IP = "Empty"
      }
      if (r.VOIP_IP.Valid) {
          k.VOIP_IP = r.VOIP_IP.String
      } else {
          k.VOIP_IP = "Empty"
      }
      if (r.Local_IP.Valid) {
          k.Local_IP = r.Local_IP.String
      } else {
          k.Local_IP = "Empty"
      }
      if (r.status.Valid) {
          str := strconv.FormatInt(r.status.Int64, 10) // use base 10 for sanity purpose
          k.Status = str
      } else {
          k.Status = "Empty"
      }
      k_box.Items = append(k_box.Items, k)
  }
}

func (l_box *LoesningBox) GetLoesning(flexfone_db *sql.DB) {
  rows, err := flexfone_db.Query(`SELECT
                          loesninger_pbx_id,Kredsloebsnummer,aktiv
                          FROM flexfone_dk_db.loesninger_internet
                          WHERE kredsloebsnummer is not null;`)
  defer rows.Close()
  checkErr(err)
  for rows.Next() {
      var r l_row
      var l Loesning
      err := rows.Scan(&r.Loesninger_pbx_id, &r.Kredsloebsnummer, &r.Aktiv)
      checkErr(err)
      if (r.Loesninger_pbx_id.Valid) {
          l.Loesninger_pbx_id = r.Loesninger_pbx_id.String
      } else {
          l.Loesninger_pbx_id = "Empty"
      }
      if (r.Kredsloebsnummer.Valid) {
          l.Kredsloebsnummer = r.Kredsloebsnummer.String
      } else {
          l.Kredsloebsnummer = "Empty"
      }
      if (r.Aktiv.Valid) {
          l.Aktiv = r.Aktiv.String
      } else {
          l.Aktiv = "Empty"
      }
      l_box.Items = append(l_box.Items, l)
  }
}
