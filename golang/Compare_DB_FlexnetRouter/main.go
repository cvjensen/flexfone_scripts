package main

import "gopkg.in/natefinch/lumberjack.v2"
import "fmt"
import "log"

type error interface {
    Error() string
}

func main() {
  //Open error log
  log.SetOutput(&lumberjack.Logger{
      Filename:   "compare_db_flexnetrouter.log",
      MaxSize:    10, // megabytes
      MaxBackups: 3,
      MaxAge:     28, //days
  })
  fmt.Println("Here we go...")

  //Define objects
  k_items := []Kredsloeb{}
  k_box := KredsloebBox{k_items}

  l_items := []Loesning{}
  l_box := LoesningBox{l_items}

  arp_items := []ArpEntry{}
  arp_box := ArpEntryBox{arp_items}

  arp_vrf_items := []ArpEntry{}
  arp_vrf_box := ArpEntryBox{arp_vrf_items}

  dhcp_items := []DhcpEntry{}
  dhcp_box := DhcpEntryBox{dhcp_items}

  static_route_items := []StaticRouteEntry{}
  static_route_box := StaticRouteEntryBox{static_route_items}

  //Connect databases
  flexnet_db := connectFlexnetDb()
  flexfone_db := connectFlexfoneDb()

  //Get Kredsloeb
  k_box.GetKredsloeb(flexnet_db)

  //Get Loesning
  l_box.GetLoesning(flexfone_db)

  //Get ARP
  arp_box.GetShowArp("/home/cj/show_arp.txt")

  //Get ARP for VOIP
  arp_vrf_box.GetShowArp("/home/cj/show_arp_vrf_voip.txt")

  //Get DHCP
  dhcp_box.GetShowDhcp("/home/cj/show_dhcp_ipv4.txt")

  //Get static routes
  static_route_box.GetShowRouteVrf("/home/cj/show_route_vrf_voip.txt")

  //Print some stuff on screen
  fmt.Println("Database Kredsloeb count:", len(k_box.Items))
  fmt.Println("Database Loesning count:", len(l_box.Items))
  fmt.Println("ARP Flexnetrouter Entries:", len(arp_box.Items))
  fmt.Println("ARP VRF Flexnetrouter Entries:", len(arp_vrf_box.Items))
  fmt.Println("DHCP Flexnetrouter Entries:", len(dhcp_box.Items))
  fmt.Println("Static Flexnetrouter Entries:", len(static_route_box.Items))

  //Close database connections
  defer flexnet_db.Close()
  defer flexfone_db.Close()

  //Export data
  if len(k_box.Items) > 0 && len(l_box.Items) > 0 && len(arp_box.Items) > 0 && len(arp_vrf_box.Items) > 0 && len(dhcp_box.Items) > 0 && len(static_route_box.Items) > 0 {
    drift_db := connectDriftDb()

    //Export to drift database
    exportKredsloeb(k_box, drift_db)
    exportLoesning(l_box, drift_db)
    exportArpEntries(arp_box, drift_db)
    exportArpVrfEntries(arp_vrf_box, drift_db)
    exportDhcp(dhcp_box, drift_db)
    exportStaticRoutes(static_route_box, drift_db)

    //Select from drift database views and get errors formatted as HTML
    error_count := GetErrors(drift_db)
    defer drift_db.Close()

    //Some more output to screen
    fmt.Println("Errors count: ", error_count)
  }
}

//Error checking method
func checkErr(err error) {
    if err != nil {
        SendError(err.Error())
        log.Fatalf("Fatal error when comparing flexnet router to database: %v\n", err.Error())
    }
}
