package main

import "database/sql"
import _ "github.com/go-sql-driver/mysql"

func connectDriftDb() (*sql.DB)  {
  db, err := sql.Open("mysql", "flexnet:HSUJK87jklj@tcp(siptrace.sippeer.dk:3306)/drift")
  checkErr(err)
  return db
}

func connectFlexnetDb() (*sql.DB)  {
  db, err := sql.Open("mysql", "flexfone:Lr3VDrSHp2drU35P@tcp(db02.sippeer.dk:3306)/flexnet_db")
  checkErr(err)
  return db
}

func connectFlexfoneDb() (*sql.DB)  {
  db, err := sql.Open("mysql", "flexfone:Lr3VDrSHp2drU35P@tcp(db02.sippeer.dk:3306)/flexfone_dk_db")
  checkErr(err)
  return db
}
